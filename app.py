from flask import Flask, render_template, url_for, request, redirect
from flask_sqlalchemy import SQLAlchemy
from urllib.request import urlopen
from datetime import datetime
from xml.etree import ElementTree as etree
import calendar

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///app.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


class Data(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30), nullable=False)
    prefix = db.Column(db.String(2), nullable=False)
    price = db.Column(db.Integer, nullable=False)
    date = db.Column(db.Date, default=datetime.utcnow)


def __repr__(self):
    return '<Data>' % self.id


@app.route('/', methods=['POST', 'GET'])
def index():
    today = datetime.today()
    month_days = calendar.monthrange(today.year, today.month)
    metal = f'https://www.cbr.ru/scripts/xml_metall.asp?date_req1=01/{today.month}/{today.year}&date_req2={month_days[1]}/{today.month}/{today.year}'
    with urlopen(metal) as r:
        au_s = etree.parse(r).findtext('.//Record[@Code="1"]/Sell')
    with urlopen(metal) as r:
        ag_s = etree.parse(r).findtext('.//Record[@Code="2"]/Sell')
    with urlopen(metal) as r:
        pt_s = etree.parse(r).findtext('.//Record[@Code="3"]/Sell')
    with urlopen(metal) as r:
        pd_s = etree.parse(r).findtext('.//Record[@Code="4"]/Sell')

    with urlopen(metal) as r:

        main_data = etree.parse(r)
        root = main_data.getroot()

        for record in root.findall('Record'):
            sell = record.find('Sell').text
            code = record.get('Code')

        return render_template("index.html", **locals())


@app.route('/dashboard')
def dashboard():
    return render_template("admin.html")


@app.route('/get', methods=['POST', 'GET'])
def get():
    if request.method == "POST":
        name = request.form['metal_name']
        prefix = request.form['metal_prefix']
        price = request.form['metal_price']

        data = Data(name=name, prefix=prefix, price=price)

        try:
            db.session.add(data)
            db.session.commit()
            return redirect('/get')
        except:
            return "... something went wrong"
    else:
        return render_template("get.html")


@app.route('/view')
def view():
    data_list = Data.query.order_by(Data.date.desc()).all()
    return render_template("view.html", data_list=data_list)


if __name__ == "__main__":
    app.run(debug=True)